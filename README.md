## Colourized

> Colourized dataset registry.

A DVC dataset registry containing datasets which have been preprocessed into
many different colourspaces using colourize.
